import openpyxl
import imaplib
import email
import sys
import re
import smtplib

#------------------------------------------------
#Please enter your gmail id and password to continue. If other than gmail is used, please consider changing the imap address.
acc = "######@gmail.com"
passcode = "#########"
#------------------------------------------------

#------------------------------------IMPORTANT---------------------------------
#You need o have a email with the subject "Request for bank statement", to get balance results onto your email ID.
#Besides please update the details in Excel sheet and its path provided to get valid details without errors.
excel_path = "airline.xlsx"

folder = "INBOX"
dict = {}
regex = r'\<(.*?)\>'


def process_mailbox(m):
    global emailname, sub
    emailname = ""
    print "Reading all emails in INBOX..."
    rv, data = m.search(None, "ALL")
    cont = data[0]
    cont = cont.split(" ")
    if rv != "OK":
        print "No messages found"
        return
    print "\nThere are total of %s emails to be read." % len(cont)
    m.select("INBOX", readonly=True)
    for i in range(1, len(cont) + 1):
        print "Searching %s email" % i
        typ, msg_data = m.fetch(str(i), '(RFC822)')
        for res in msg_data:
            if isinstance(res, tuple):
                msg = email.message_from_string(res[1])
                for header in ["subject", "from"]:
                    count = 0
                    if header == "subject":
                        sub = msg[header]
                        print sub
                    if sub == "Request for bank statement":
                        if header == "from":
                            emailname = msg[header]
                            temp = re.findall(regex, emailname)
                            if temp:
                                emailname = temp[0]
                            temp = None
                        if emailname != "":
                            if emailname not in dict:
                                dict[emailname] = count + 1
                            else:
                                dict[emailname] += 1


def reading_data():
    data = openpyxl.load_workbook(excel_path)
    sheet = data.get_sheet_by_name("Sheet1")
    maxr = sheet.max_row
    maxc = sheet.max_column
    arr = []

    for i in xrange(2, maxr):
        cont = sheet.cell(row=i, column=1).value, sheet.cell(row=i, column=2).value, sheet.cell(row=i,
                                                                                                column=3).value, sheet.cell(
            row=i, column=4).value
        if None not in cont:
            cont = [str(r) for r in cont]
            arr.append(cont)

    return arr


def check_data(arr_data):
    email_name = []
    pos = 0
    for item in arr_data:
        email_name.append(item[1])
    if not dict:
        return "empty"
    for names in dict:
        count = 0
        if names not in email_name:
            count = dict[names]
            dict[names] = [count, False]
        else:
            count = dict[names]
            for item in arr_data:
                if names in item:
                    pos = arr_data.index(item)
            dict[names] = [count, arr_data[pos]]

    return dict


def protocol(data_name, data_email, data_id, data_balance, message):
    try:
        server = smtplib.SMTP(host='smtp.gmail.com', port=587)
        server.ehlo()
        server.starttls()

        sub = "Reply to request for bank statement"
        # Login to server
        server.login(acc, passcode)

        # Send the mail
        msg = 'Subject: {}\n\n{}'.format(sub, message)
        server.sendmail(acc, data_email, msg)
        print "\nEmail sent sucessfully to %s..." %data_email
    except:
        print "Email not sent"


def dispatcher(dict_final):
    if dict_final == "empty":
        print "\n\nThere is no request for balance verification."
    if dict_final:
        for names in dict_final:
            count = dict_final[names][0]
            data = dict_final[names][1]
            if data == False:
                data_email = names
                data_name = data_id = data_balance = False
                message = ("Hai %s\n\n"
                           "You are not a registered user, so this is termed as an unauthorised transaction") % data_email
                protocol(data_name, data_email, data_id, data_balance, message)

            if data:
                data_name = data[0]
                data_email = data[1]
                data_id = data[2]
                data_balance = data[3]
                message = ("Hai %s\n\n"
                           "Below are the details of your bank statement as requested:\n"
                           "Customer id\t: %s\n"
                           "Balance\t\t: %s\n") % (data_name, data_id, data_balance)
                protocol(data_name, data_email, data_id, data_balance, message)


# -----------------------------------------------------------------------------------------------------------------------

m = imaplib.IMAP4_SSL('imap.gmail.com')
try:
    print "Connecting to Gmail...\n"
    rv, data = m.login(acc, passcode)
except:
    print "Login failed"
    sys.exit(1)

rv, data = m.select(folder)
if rv == "OK":
    process_mailbox(m)
    m.close()
else:
    print "\nError: Unable to process"

print "\nReading customers data for verifying..."
details = reading_data()

print "\nChecking the requests with internal data"
checker = check_data(details)

dispatcher(checker)
